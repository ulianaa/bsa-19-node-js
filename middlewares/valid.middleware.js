/* middlewares/valid.middleware.js */

const isNumeric = (n) => {
  return !isNaN(parseFloat(n)) && isFinite(n) && n >= 0;
}

const isCorrect = (req, res, next) => {
    if (
      req &&
      req.body &&
      req.body.name &&
      isNumeric(req.body.health) &&
      isNumeric(req.body.attack) &&
      isNumeric(req.body.defense) &&
      req.body.source 
    ) {
      next();
    } else {
      res.status(400).send('Invalid data');
    }
  };
  
  module.exports = {
    isCorrect
  }