/* repositories/user.repository.js */

const path = require('path');
const fs = require('fs');

const filename = path.normalize("./data/userlist.json");

let usersJSON = fs.readFileSync(filename);
let users = JSON.parse(usersJSON);

//функция для сохранения нового юзера
const saveUser = (data) => {
    if (data) {
      data._id = String(users.length + 1); 
      users.push(data);
      rewriteFile();
      return true;
    } else {
      return false;
    }
};

//функция для удаления юзера
const deleteUser = (id) => {
  if (users[id - 1]) {
    users.splice(id - 1, 1);
    users.forEach((el, i) => {
      el._id = String(i + 1);
    });
    rewriteFile();
    return true;
  } else {
    return false;
  }
};

//функция для обновления юзера
const modifyUser = (id, data) => {
  if (users[id - 1]) {
    users[id - 1] = data;
    users[id - 1]._id = String(id);
    rewriteFile();
    return true;
  } else {
    return false;
  }
}

//функция для получения всех юзеров
const getUsers = () => {
  if (users) {
    console.log(filename);
    return users; 
  } else {
    return false;
  }
};

const rewriteFile = () => {
  fs.writeFileSync(filename, JSON.stringify(users, null, 2));
  usersJSON = fs.readFileSync(filename);
  users = JSON.parse(usersJSON);
  console.log(users);
}
  
  module.exports = {
    saveUser,
    getUsers,
    deleteUser,
    modifyUser
  };