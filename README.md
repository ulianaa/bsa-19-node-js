# bsa-19, node js
## street-fighters simple server 
*The server is deployed here https://bsa-19-node-js.herokuapp.com*

### How to install: 
1. ```npm install```
2. ```nodemon start```

### There are such dependencies: 
*  [express](https://expressjs.com/)
*  [fs](https://nodejs.org/api/fs.html#fs_file_system)
*  [body-parser](https://www.npmjs.com/package/body-parser)
*  [handlebars](https://www.npmjs.com/package/handlebars)
*  [nodemon](https://www.npmjs.com/package/nodemon) 
*  [morgan](https://www.npmjs.com/package/morgan)
*  [path](https://www.npmjs.com/package/path)
	

### The server can handle 5 types of requests: 
1. ```GET: /user``` - to get all users
2. ```GET: /user/:id``` - to get one user
3. ```POST: /user``` - to add a user
4. ```PUT: /user/:id``` - to modify a user
5. ```DELETE: /user/:id``` - to delete a user



