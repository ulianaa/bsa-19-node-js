/* routes/user.js */

const express = require('express');
const router = express.Router();

const { getUsers, saveUser, deleteUser, modifyUser } = require("../repositories/user.repository");
const { getUserById, showUsers } = require("../services/user.service");
const { isCorrect } = require("../middlewares/valid.middleware");

router.get('/', function(req, res, next) {
  const result = getUsers();
  if (result) res.render('../views/index.hbs', {
    users: result
  });
  else res.status(404).send('There are not any users');
});

router.get('/:id', function(req, res, next) { 
  const id = req.params.id;                   
  const result = getUserById(id);
  if (result) res.render('../views/index.hbs', {
    users: [result]
  });
  else res.status(400).send(`No user with id = ${id}`);
});

router.post('/', isCorrect, function(req, res, next) { 
  const result = saveUser(req.body);       
  if (result) res.send(`User №${req.body._id} was added successfully`);
  else res.status(400).send(`Invalid data`);
});

router.delete('/:id', function(req, res, next) {
  const id = req.params.id;
  const result = deleteUser(id);
  if (result) res.send(`User №${id} was deleted successfully`);
  else res.status(400).send(`No user with id = ${id}`);
});

router.put('/:id', isCorrect, function(req, res, next) { 
  const id = req.params.id;                   
  const result = modifyUser(id, req.body);
  if (result) res.send(`User №${id} was modified successfully`);
  else res.status(400).send(`No user with id = ${id}`);
});

module.exports = router;